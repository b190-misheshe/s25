console.log("Hello World");

// SECTION -JSON Objects
/*
	- JSON stands for JavaScript Object Notation
	- JSON is also used in other programming language, hence the "Notation" in its name
	- JSON is not to be confused with Javascript objects since aside from format itself, JS objects are exclusive to javascript while JSON can be used by other languages as well
	SYNTAX:
		{
			"propertyA": "valueA",
			"propertyB": "valueB"
		}
*/

/*
	Miniactivity
		create ng "cities" array of 3 JS objects with the following properties
			city
			province
			country
		[ {}, {}, {} ]
		log in the console the output
		send the output in the google chat
*/
// below is an example of array of JS object
/*let cities = [
	{city: "Quezon City", province: "Manila", country: "Philippines"},
	{city: "Makati City", province: "Manila", country: "Philippines"},
	{city: "Manila City", province: "Manila", country: "Philippines"},
];*/

/*
below is an example of JSON Array
	when logged in the console, it seems that the appearance of JSON does not differ from JS Object. but since JSON is used by other languages as well, it is common to see it as JS object since it is teh blueprint of the JSON format
*/
/*
	WHAT JSON DOES
		- JSON is used for serializing different data types into bytes
		- bytes is a unit of data that is composed of eight binary digits (1/0) that is used to represent a character (letters, numbers, typographical symbol)
		- serialization is the process of converting data into a series of bytes for easier transmission/transfer of information
*/
let cities = [
	{"city": "Quezon City", "province": "Manila", "country": "Philippines"},
	{"city": "Makati City", "province": "Manila", "country": "Philippines"},
	{"city": "Manila City", "province": "Manila", "country": "Philippines"},
];

console.log(cities);

// JSON Methods
	// JSON object contains methods for parsing and converting data into/from JSON or stringified JSON

// SECTION - Stringify method
	/*
		- stringified JSON is a JS object converted into a string (but in JSON format) to be used in other functions of a JS application
		- they are commonly used in HTTP requests where information is required to be sent and received ins a stringified JSON format
		- requests are an important part of programming where a frontend application communicates with a backend application to perform different tasks such getting/creating data in a database
		- a front application is an application that used to interact with users to perform different tasks and display information while the backend application are commonly used for all business logic and data processing
		- a database normally stores information/data that can be used in most application
		- commonly stored data in databases are user information, transaction records, and product information
		- Node/Express JS are two of technologies that are used for creating backend applications which process requests from frontend application
			- Node JS is a Java Runtime Environment (JRE)/ software that is made to execute other software
			- Express JS is a Node JS framework that provides features for easily creating web and mobile applications
	*/
let batchesArr = [ { batchName: "Batch X" }, { batchName: "Batch Y" } ];
console.log(batchesArr);
// stringify method - used to convert JS objects into a stringified JSON
console.log("Result from stringify method");
console.log(JSON.stringify(batchesArr));

/*
	Miniactivity:
		create a "data" JS object
			name: string
			age: number
			address: object{
				city: string
				coutnry: string
			}
		log the object in the console, but make it stringified JSON
		send the screenshot in the chat
*/
// direct conversion of data type from JS object into stringified JSON 
let data = JSON.stringify({
	name: "John",
	age: 31,
	address:{
		city: "Batangas",
		country: "Philippines"
	}
});

console.log(data);

// an example where JSON is commonly used is on package.json files which an express JS application uses to keep track of the information regarding a repository/project (see package.json)


// JSON.stringify with variables and prompt()
/*
	- When information is stored in a variable and is not hard coded into an object that is being stringified, we can suppply the value with a variable
	- the "property" name and the "value" name would have to be the same and this might be a little confusing, but this is to signify that the description of the variable and the property name pertains to the same thing and that is the value of the variable itself
	- this also helps in code readability
	- this is commonly used when we try to send information to the backend application
	- SYNTAX:
			JSON.stringify({
				propertA: variableA,
				propertB: variableB
			})
	- since we do not have a frontend application yet, we will use the prompt method in order to gather user data
*/
let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
	city: prompt("Which city do you live?"),
	country: prompt("Which country does your city belong?")
}

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address
})

console.log(otherData);

// parse method
/*
	- converts the stringified JSON into Javascript Objects
	- Objects are common data types used in applications because of the complex data structures that can be created out of them
	- information is commonly sent to applications in stringinfied JSON and then converted back into objects
	- this hanppens for both sending information to a backend application and sending information back to a frontend application
*/
let batchesJSON = `[ { "batchName" : "Batch X" }, { "batchName": "Batch Y" } ]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));
/*
	Miniactivity
		log in the console the parsed data coming from the "data" and "otherData" variables

		send the output in the google chat
*/
console.log("Result from parse method");
console.log(JSON.parse(data));
console.log(JSON.parse(otherData));
