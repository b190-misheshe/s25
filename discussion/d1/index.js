console.log("Ho World!");

// JSON = Javascript Object Notation
// use is not limited to Javascript, can be used in other programming languages, hence, notation in its name

// let cities = [{
//   city: "General Trias",
//   province: "Cavite",
//   country: "Philippines"
// },{
//   city: "Pasig City",
//   province: "Manila",
//   country: "Philippines"
// },{
//   city: "San Carlos City",
//   province: "Negros Occidental",
//   country: "Philippines"
// }
// ];

// console.log(cities);

// >>>>> JSON <<<<<
// no diff with JS object when logged in the console

// What JSON does?
// used for serializing different data types into bytes
// bytes

// cities = [
//   {"city": "General Trias", "province": "Cavite", "country": "Philippines"},
//   {"city": "Pasig City", "province": "Manila", "country": "Philippines"},
//   {"city": "San Carlos City", "province": "Negros Occidental", "country": "Philippines"}
// ];

// console.log(cities);

// >>>>> JSON Methods <<<<<

// SECTION : Stringify method


// stringified json - converted, from js object into string (in json format)
// to be used in other functions of a js app
// commonly used in http request where info is required to be sent and received in a stringified json format (request from user or api)

// ^^^^^^^^^
// let batchesArr = [{batchName: "Batch X"}, {batchName: "Batch Y"}];
// console.log(batchesArr);
// console.log("Result from stringify method");
// console.log(JSON.stringify(batchesArr));
// console.log(typeof batchesArr);
// console.log(typeof JSON.stringify(batchesArr));

// 1.one way to stringify

// let data = {
//   name: "Steve Smith",
//   age: 30, 
//   address: {
//     city: "London",
//     country: "England"
//   }
// };
// console.log(data);
// console.log(JSON.stringify(data));
// const stringData = JSON.stringify(data);
// console.log(stringData);

// 2. another way to stringify
// data = JSON.stringify ({
//   name: "Steve Smith",
//   age: 30, 
//   address: {
//     city: "London",
//     country: "England"
//   }
// });
// console.log(data);

// ** JSON is commonly used on package.json

// *** JSON.stringify with prompt()

// ^^^^^^^^^^
let fname = prompt("What is your first name?");
let lname = prompt("What is your last name?");
let age = prompt("What is your age?");
let address = {
  city: prompt("Which city do you live?"),
  country: prompt("Which country does your city belong?"),
}

let otherData = JSON.stringify({
	fname: fname,
	lname: lname,
	age: age,
	address: address
});

console.log(otherData);


/* Example only to check (Cannot be inside each string)
fname = JSON.stringify(prompt("What is your first name?"));
lname = JSON.stringify(prompt("What is your last name?"));
console.log(fname);
console.log(lname);

let otherData = JSON.stringify({
	fname: fname,
	lname: lname
});
console.log(otherData);

otherData = ({
	fname: fname,
	lname: lname
});
console.log(otherData);
*/

// *** parse method

let batchesJSON = `[{"batchName": "Batch X"},{batchName: "Batch "Y}]`;
console.log(batchesJSON);
console.log("Result from parse method");
console.log(JSON.parse(batchesJSON));

// Miniactivity - to JSON.parse previous example
// stringify before you can do parse

// console.log("Result from sample parse method");
// console.log(JSON.parse(data));
// console.log(JSON.parse(otherData));

